function switch_project --description 'Switch to a project directory'
    set dir (find-project $argv)

    if test $status -eq 0
        cd $dir
    else
        echo 'No project selected.'
    end
end
