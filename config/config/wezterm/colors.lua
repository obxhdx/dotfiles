local wezterm = require 'wezterm'

local BASE_THEME = 'Catppuccin Macchiato'
local colors = wezterm.color.get_builtin_schemes()[BASE_THEME]
local blue = colors.brights[5]

colors.tab_bar = {
  background = colors.background,
  active_tab = {
    bg_color = colors.background,
    fg_color = colors.foreground,
  },
  inactive_tab = {
    bg_color = colors.background,
    fg_color = blue,
  },
  inactive_tab_hover = {
    bg_color = colors.background,
    fg_color = blue,
  },
}

return colors
