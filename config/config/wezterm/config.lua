local wezterm = require 'wezterm'
local act = wezterm.action

local colors = require 'colors'
local keys = require 'keys'

local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- Colors
config.color_schemes = {
  ['Custom'] = colors,
}
config.color_scheme = 'Custom'

-- Environment
local home_bin_path = os.getenv 'HOME' .. '/.bin/'
local homebrew_bin_path = '/opt/homebrew/bin/'
local path = string.format('%s:%s:%s', home_bin_path, homebrew_bin_path, os.getenv 'PATH')

config.set_environment_variables = {
  PATH = path,
  SHELL = homebrew_bin_path .. 'fish',
}

-- Font
config.font = wezterm.font('OperatorMono NF', { weight = 'Book' })
config.font_size = 14.0
config.underline_thickness = 3
config.underline_position = -4

-- Keys
config.keys = keys

-- Misc
config.default_cursor_style = 'SteadyUnderline'

-- Mouse behavior
config.mouse_bindings = {
  -- Select the complete output from a command
  {
    event = { Down = { streak = 4, button = 'Left' } },
    action = act.SelectTextAtMouseCursor 'SemanticZone',
    mods = 'NONE',
  },

  -- Change the default click behavior so that it only selects
  -- text and doesn't open hyperlinks
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'NONE',
    action = act.CompleteSelection 'ClipboardAndPrimarySelection',
  },

  -- and make CTRL-Click open hyperlinks
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'CTRL',
    action = act.OpenLinkAtMouseCursor,
  },
}

-- Tab bar
config.tab_bar_at_bottom = true
config.tab_max_width = 50
config.use_fancy_tab_bar = false
config.show_new_tab_button_in_tab_bar = false

-- Window appearance
config.initial_cols = 115
config.initial_rows = 35
config.window_padding = {
  left = 0,
  right = 0,
  top = 0,
  bottom = 0,
}

return config
