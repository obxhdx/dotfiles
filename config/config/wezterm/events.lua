local wezterm = require 'wezterm'

local colors = require 'colors'
local helpers = require 'helpers'
local icons = wezterm.nerdfonts

--- Format the zoom icon for a tab.
-- This function checks if the active pane in a tab is zoomed and returns
-- the specified icon with padding if it is zoomed, or three spaces if it is not.
-- @param tab The tab object, that contains the active pane.
-- @param icon An optional icon override to display when the pane is zoomed.
-- @return A string representing the formatted icon or padding based on the zoom state.
local format_zoom_icon = function(tab, icon)
  icon = icon or icons.cod_multiple_windows
  return tab.active_pane.is_zoomed and ' ' .. icon .. ' ' or '   '
end

--- Event handler for formatting the title of windows.
-- @param tab The tab object for which the title is being formatted.
-- @return A string representing the formatted title.
---@diagnostic disable-next-line: unused-local
wezterm.on('format-window-title', function(tab, pane, tabs, panes, config)
  local index = ''
  if #tabs > 1 then
    index = string.format('(%d/%d) ', tab.tab_index + 1, #tabs)
  end

  return format_zoom_icon(tab, '(Z)') .. index .. helpers.format_process_name(tab)
end)

--- Event handler for formatting the title of tabs.
-- @param tab The tab object for which the title is being formatted.
-- @return A table representing the formatted title components.
wezterm.on('format-tab-title', function(tab)
  return wezterm.format {
    -- Tab index
    { Text = ' ' },
    { Attribute = { Italic = true } },
    { Foreground = { Color = 'Grey' } },
    { Text = string.format('%s ', tab.tab_index + 1) },
    { Text = ' ' },
    'ResetAttributes',

    -- Process icon
    { Text = helpers.format_process_icon(tab) },

    -- Folder icon
    { Foreground = { AnsiColor = 'Silver' } },
    { Text = wezterm.nerdfonts.custom_folder_open },
    { Text = ' ' },
    'ResetAttributes',

    -- Current dir
    { Attribute = { Italic = true } },
    { Text = helpers.get_current_working_dir(tab) },
    { Text = format_zoom_icon(tab) },
  }
end)

--- Event handler to reload files when Helix editor regains focus.
-- @param window The window object containing the pane.
-- @param pane The pane object to check for the foreground process.
wezterm.on('helix-reload-file', function(window, pane)
  local fg_process = helpers.basename(pane:get_foreground_process_name())

  -- Checks if the foreground process of the pane is 'hx' (Helix)
  if fg_process == 'hx' then
    local act = wezterm.action
    window:perform_action(act.SendString '\x1b', pane) -- leave insert mode
    window:perform_action(act.SendString ':reload-all\r\n', pane) -- trigger a reload action in Helix
  end
end)

--- Event handler for opening URIs.
-- @param window The window object where the URI was activated.
-- @param pane The pane object where the URI was activated.
-- @param uri The activated URI.
wezterm.on('open-uri', function(window, pane, uri)
  local cmd = 'hx'
  local direction = 'Down'

  -- Function to check if the URI is a URL
  local function is_url(_uri)
    return _uri:match '^http[s]?://'
  end

  if not is_url(uri) then
    -- Retrieve a pane in the specified direction relative to the current one.
    local helix_pane = pane:tab():get_pane_direction(direction)

    if helix_pane == nil then
      -- If there is no pane in the direction, split the pane and open the URI with the command.
      local action = wezterm.action {
        SplitPane = {
          direction = direction,
          command = { args = { cmd, uri } },
        },
      }
      window:perform_action(action, pane)

      -- prevent the default action from opening in a browser
      return false
    else
      -- If there is already a pane, send the command to open the URI in that pane.
      local action = wezterm.action.SendString(':open ' .. uri .. '\r\n')
      window:perform_action(action, helix_pane)
    end
  end

  -- Otherwise, by not specifying a return value, we allow later
  -- handlers or default action to open the URI in a browser.
end)

--- Event handler for updating the right-side status
-- with the current date and time.
-- @param window The window object to update the status on.
wezterm.on('update-right-status', function(window)
  local date = wezterm.strftime '%a %b %e %l:%M %p '

  window:set_right_status(wezterm.format {
    { Attribute = { Italic = true } },
    { Foreground = { Color = colors.brights[5] } },
    { Text = date .. wezterm.nerdfonts.md_calendar_clock .. ' ' },
  })
end)
