local wezterm = require 'wezterm'
local icons = wezterm.nerdfonts

local M = {}

-- Equivalent to POSIX basename(3)
-- Given "/foo/bar" returns "bar"
-- Given "c:\\foo\\bar" returns "bar"
function M.basename(s)
  return string.gsub(s, '(.*[/\\])(.*)', '%2')
end

local function url_decode(str)
  return string.gsub(str, '%%(%x%x)', function(hex)
    return string.char(tonumber(hex, 16))
  end)
end

function M.format_process_name(tab)
  -- Strip the path from the process name
  local process_name = string.gsub(tab.active_pane.foreground_process_name, '(.*[/\\])(.*)', '%2')

  -- Strip any version numbers from the process name
  return string.gsub(process_name, '(%a+)[%d%.]*$', '%1')
end

--- Returns the formatted current working directory of the active pane in a tab.
-- @param tab Tab object.
-- @return String representing the current directory.
function M.get_current_working_dir(tab)
  local current_dir = url_decode(tab.active_pane.current_working_dir.path)
  local home_dir = os.getenv 'HOME'
  return current_dir == home_dir and '~' or M.basename(current_dir)
end

--- Retrieves icon and color formatting for the active process in a tab.
-- @param tab Tab object.
-- @return Formatted icon for the active process.
function M.format_process_icon(tab)
  local function make_icon_table(color, icon, right_padding)
    return {
      { Foreground = { Color = color } },
      { Text = icon },
      { Text = right_padding or ' ' },
    }
  end

  local empty_icon = { { Text = '' } }
  local process_icons = {
    ['bash'] = make_icon_table('Grey', icons.dev_terminal),
    ['docker-compose'] = make_icon_table('Aqua', icons.linux_docker),
    ['git'] = make_icon_table('Orange', icons.md_git, '  '),
    ['hx'] = make_icon_table('None', '🧬'),
    ['node'] = make_icon_table('Lime', icons.dev_nodejs_small),
    ['nvim'] = make_icon_table('LightGreen', icons.custom_neovim, '  '),
    ['python'] = make_icon_table('Yellow', icons.md_snake, '  '),
    ['vim'] = make_icon_table('Green', icons.dev_vim),
    ['zsh'] = make_icon_table('Grey', icons.dev_terminal),
  }

  local process_name = M.format_process_name(tab)
  return wezterm.format(process_icons[process_name] or empty_icon)
end

--- Creates a keymap for WezTerm to interact with Helix editor.
-- This function generates a keymap that includes a primary action
-- (like pane navigation or tab control) and also emits an event
-- to reload the file in Helix editor. It's designed to work with
-- specific actions tailored for Helix integration.
-- @param mods Modifier keys used in the keymap (e.g., 'SUPER').
-- @param key The key to be mapped.
-- @param action A table defining the action to be performed. The first
-- element is the action type, and the second element is the action value.
-- @return A table representing the keymap configuration for WezTerm.
function M.create_helix_interactive_keymap(mods, key, action)
  local act = require('wezterm').action
  local main_action

  if type(action) == 'table' and action[1] == 'ActivatePaneDirection' then
    main_action = act.ActivatePaneDirection(action[2]) -- hjkl
  elseif type(action) == 'table' and action[1] == 'CloseCurrentPane' then
    main_action = act.CloseCurrentPane { confirm = true }
  elseif type(action) == 'table' and action[1] == 'CloseCurrentTab' then
    main_action = act.CloseCurrentTab { confirm = true }
  elseif type(action) == 'table' and action[1] == 'ActivateTab' then
    main_action = act.ActivateTab(action[2])
  end

  return {
    mods = mods,
    key = key,
    action = act.Multiple {
      main_action,
      act.EmitEvent 'helix-reload-file',
    },
  }
end

return M
