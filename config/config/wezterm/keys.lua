-- Default Key Assignments https://wezfurlong.org/wezterm/config/default-keys.html

local wezterm = require 'wezterm'
local act = wezterm.action

local create_helix_interactive_keymap = require('helpers').create_helix_interactive_keymap

return {
  -- Cycle throuth the start of an earlier/later command
  { mods = 'SHIFT', key = 'UpArrow', action = act.ScrollToPrompt(-1) },
  { mods = 'SHIFT', key = 'DownArrow', action = act.ScrollToPrompt(1) },

  -- Misc
  { mods = 'SUPER', key = 'Enter', action = act.ToggleFullScreen },
  { mods = 'SUPER', key = 'z', action = act.TogglePaneZoomState },

  -- Creating panes
  { mods = 'SUPER', key = 'D', action = act { SplitVertical = { domain = 'CurrentPaneDomain' } } },
  { mods = 'SUPER', key = 'd', action = act { SplitHorizontal = { domain = 'CurrentPaneDomain' } } },

  -- Navigating panes
  create_helix_interactive_keymap('SUPER', 'h', { 'ActivatePaneDirection', 'Left' }),
  create_helix_interactive_keymap('SUPER', 'j', { 'ActivatePaneDirection', 'Down' }),
  create_helix_interactive_keymap('SUPER', 'k', { 'ActivatePaneDirection', 'Up' }),
  create_helix_interactive_keymap('SUPER', 'l', { 'ActivatePaneDirection', 'Right' }),
  create_helix_interactive_keymap('SUPER', 'w', { 'CloseCurrentPane' }),
  create_helix_interactive_keymap('SUPER', '1', { 'ActivateTab', 0 }),
  create_helix_interactive_keymap('SUPER', '2', { 'ActivateTab', 1 }),
  create_helix_interactive_keymap('SUPER', '3', { 'ActivateTab', 2 }),
  create_helix_interactive_keymap('SUPER', '4', { 'ActivateTab', 3 }),
  create_helix_interactive_keymap('SUPER', '5', { 'ActivateTab', 4 }),
  create_helix_interactive_keymap('SUPER', '6', { 'ActivateTab', 5 }),
  create_helix_interactive_keymap('SUPER', '7', { 'ActivateTab', 6 }),
  create_helix_interactive_keymap('SUPER', '8', { 'ActivateTab', 7 }),
  create_helix_interactive_keymap('SUPER', '9', { 'ActivateTab', 8 }),

  -- Cycling through tabs
  { mods = 'SUPER|ALT', key = 'LeftArrow', action = act { ActivateTabRelative = -1 } },
  { mods = 'SUPER|ALT', key = 'RightArrow', action = act { ActivateTabRelative = 1 } },

  -- Moving tabs
  { mods = 'SUPER|SHIFT', key = 'LeftArrow', action = act { MoveTabRelative = -1 } },
  { mods = 'SUPER|SHIFT', key = 'RightArrow', action = act { MoveTabRelative = 1 } },

  -- Fix readline "undo"
  { mods = 'CTRL', key = '/', action = act { SendString = '\x1f' } },

  -- File explorer
  {
    key = 'e',
    mods = 'CMD',
    action = wezterm.action.SplitPane {
      direction = 'Left',
      command = {
        args = {
          'fish',
          '-c',
          'open_file_explorer',
        },
      },
      size = { Percent = 25 },
    },
  },

  {
    key = 'o',
    mods = 'CMD',
    action = wezterm.action.QuickSelectArgs {
      label = 'Smart Select: Open URL or Copy Path to Clipboard',
      patterns = {
        '(?:ftp|git@|https?)[A-Za-z:./-]+', -- Match URLs
        '/[^\\s]+', -- Match Unix file paths
      },
      action = wezterm.action_callback(function(window, pane)
        local selection = window:get_selection_text_for_pane(pane)

        if selection:match '^https?://' then
          wezterm.open_with(selection)
        else
          window:copy_to_clipboard(selection)
        end
      end),
    },
  },
}
