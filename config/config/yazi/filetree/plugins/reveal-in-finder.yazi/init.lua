return {
  entry = function()
    local h = cx.active.current.hovered
    local file_path = tostring(h.url)
    os.execute('open -R ' .. file_path)
  end,
}
