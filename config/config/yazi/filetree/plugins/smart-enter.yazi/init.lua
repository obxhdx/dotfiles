--- @sync entry
return {
  entry = function()
    local h = cx.active.current.hovered

    if h.cha.is_dir then
      ya.manager_emit('enter' or 'open', { hovered = true })
    else
      local file_path = tostring(h.url)
      os.execute('yazi_open_in_helix ' .. file_path)
    end
  end,
}
