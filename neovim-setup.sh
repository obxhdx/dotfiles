#!/usr/bin/env bash

VENV_PATH="$HOME/.venvs/neovim-pynvim"

function setup_venv {
  echo "Setting up virtual environment for Neovim Python client..."

  # Create the virtual environment if it doesn't exist
  if [ ! -d "$VENV_PATH" ]; then
    python3 -m venv "$VENV_PATH"
    echo "Virtual environment created at $VENV_PATH"
  else
    echo "Virtual environment already exists at $VENV_PATH"
  fi
}

function install_or_update_pynvim {
  echo "Updating Python client for Neovim..."

  # Activate the virtual environment
  source "$VENV_PATH/bin/activate"

  # Update or install pynvim within the virtual environment
  python3 -m pip install --upgrade pynvim

  # Deactivate the virtual environment
  deactivate
}

# Always set up the venv to ensure it exists before updating
setup_venv

# Then install/update pynvim
install_or_update_pynvim
